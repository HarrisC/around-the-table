package com.aroundthetable.recipes.AroundTheTable;

import com.google.common.collect.Iterators;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;


import java.time.LocalDate;
import java.util.*;

@Service
public class SeedService {
    private static final List<String> ingredientNames = Arrays.asList("broccoli", "onion", "garlic", "avocado", "turnips", "love", "cheddar cheese", "olive oil");
    private static final List<String> ingredientUnits = Arrays.asList("cup", "teaspoon", "tablespoon", "dash", "gallon", "liter", "quart");
    private static final Lorem LOREM = LoremIpsum.getInstance();
    private final Logger logger = LoggerFactory.getLogger(SeedController.class);
    private final RecipeRepository recipeRepository;

    public SeedService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }
    public void generateRandomRecipes(int numReports) {
        List<Recipe> recipes = new ArrayList<>();
        for (int i = 0; i < numReports; i++) {
            Recipe newReport = getRandomRecipe(i);
            recipes.add(newReport);
        }

        Iterator<Recipe> reportsIterator = recipes.iterator();

        int currentRecord = 0;
        while (reportsIterator.hasNext()) {
            Iterable<Recipe> recipeChunk = () -> Iterators.limit(reportsIterator, 200);
            List<Recipe> recipeList = new ArrayList<>();
            for (Recipe recipe : recipeChunk) {
                recipeList.add(recipe);
            }


            logger.info("Saving records {}-{}", currentRecord + 1, currentRecord + recipeList.size());

            recipeRepository.saveAll(recipeList);

            currentRecord += recipeList.size();
        }

    }

    private Recipe getRandomRecipe(int index) {
        String recipeName = LOREM.getWords(2, 5);
        LocalDate created = LocalDate.now();
        String instructions = LOREM.getParagraphs(1, 5);
        List<Ingredient> ingredients = getRandomIngredients();

        return Recipe.builder().created(created).recipeName(recipeName).instructions(instructions).ingredients(ingredients).build();
    }

    private List<Ingredient> getRandomIngredients() {
        Random random = new Random();
        List<Ingredient> ingredients = new ArrayList<>();
        for (int i = 0; i < random.nextInt(5); i++) {
            ingredients.add(Ingredient.builder().quantity(random.nextDouble() * 6)
                    .unit(ingredientUnits.get(random.nextInt(ingredientUnits.size())))
                    .name(ingredientNames.get(random.nextInt(ingredientNames.size())))
                    .build());
            }
        return ingredients;
    }

    public void deleteAllRecipes() {
        recipeRepository.deleteAll();
    }
}
