package com.aroundthetable.recipes.AroundTheTable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "recipe")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String recipeName;
    private LocalDate created;
    private String instructions;
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Ingredient> ingredients;

    public static RecipeBuilder builder() {
        return new CustomRecipeBuilder();
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = new ArrayList<>();
        if (Objects.isNull(ingredients)) return;
        this.ingredients = ingredients;
        for (Ingredient ingredient: ingredients) {
            ingredient.setRecipe(this);
        }
    }

    private static class CustomRecipeBuilder extends RecipeBuilder {
        @Override
        public Recipe build() {
            Recipe recipe = super.build();
            recipe.setIngredients(recipe.ingredients);
            return recipe;
        }
    }
}
