package com.aroundthetable.recipes.AroundTheTable;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/recipes")
public class RecipesController {
    RecipesService recipesService;

    public RecipesController(RecipesService recipesService) {
        this.recipesService = recipesService;
    }

    @GetMapping("/{id}")
    Recipe getRecipeById(@PathVariable Long id) {
        return recipesService.getRecipeById(id);
    }

}
