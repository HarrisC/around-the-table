package com.aroundthetable.recipes.AroundTheTable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AroundTheTableApplication {

	public static void main(String[] args) {
		SpringApplication.run(AroundTheTableApplication.class, args);
	}

}
