package com.aroundthetable.recipes.AroundTheTable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "ingredient")
@EqualsAndHashCode(exclude = {"id", "recipe"})
@ToString(exclude = {"recipe"})
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Ingredient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double quantity;
    private String unit;
    private String name;
    @ManyToOne
    @JoinColumn(name = "recipe_id", referencedColumnName = "id", nullable = false)
    @JsonIgnore
    private Recipe recipe;

    public static IngredientBuilder builder() {
        return new CustomIngredientBuilder();
    }

    private static class CustomIngredientBuilder extends IngredientBuilder {
        @Override
        public Ingredient build() {
            Ingredient ingredient = super.build();
            return ingredient;
        }

    }
}
