package com.aroundthetable.recipes.AroundTheTable;

import org.springframework.stereotype.Service;

@Service
public class RecipesService {
    RecipeRepository recipeRepository;

    public RecipesService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    public Recipe addRecipe(Recipe recipe) {
        return recipeRepository.save(recipe);
    }

    public Recipe getRecipeById(Long id) {
        return recipeRepository.findById(id).orElse(null);
    }
}
