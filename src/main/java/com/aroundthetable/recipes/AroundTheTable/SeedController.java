package com.aroundthetable.recipes.AroundTheTable;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reseed")
public class SeedController {
    SeedService seedService;

    public SeedController(SeedService seedService) {
        this.seedService = seedService;
    }

    @GetMapping("/random")
    public String reseed(@RequestParam("numRecipes") Integer numRecipes) {
        seedService.deleteAllRecipes();
        seedService.generateRandomRecipes(numRecipes);
        return "Success, seeded " + numRecipes + " reports.";
    }

}
