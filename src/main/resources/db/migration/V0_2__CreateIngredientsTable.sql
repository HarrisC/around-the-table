CREATE TABLE  ingredient (
id INTEGER NOT NULL AUTO_INCREMENT,
recipe_id INTEGER NOT NULL,
quantity DOUBLE,
unit VARCHAR(20),
name VARCHAR(50),
PRIMARY KEY (id),
INDEX (recipe_id),
FOREIGN KEY (recipe_id) REFERENCES recipe(id)
);
