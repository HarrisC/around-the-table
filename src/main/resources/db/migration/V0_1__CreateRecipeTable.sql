CREATE TABLE  recipe (
id INTEGER NOT NULL AUTO_INCREMENT,
recipe_name varchar(100),
created DATE,
instructions TEXT,
PRIMARY KEY (id)
);
